<?php

namespace PickSuite\PickScraper\Picks;

use DateInterval;
use DOMNode;
use PickSuite\PickScraper\Cacheable;
use PickSuite\PickScraper\DOM;

class ConsensusPage extends DOM implements Cacheable
{
    const COMPETITION_EXPR = "//*[@id=\"CompetitionId\"]/@value";
    const COMPETITION_PATTERNS = '/.+/';

    /** @var string */
    public $competitionId;
    private $historical;

    public function __construct(string $html, bool $historical = false)
    {
        parent::__construct($html);
        $this->seek(static::COMPETITION_EXPR, static::COMPETITION_PATTERNS, function (DOMNode $node) {
            $this->competitionId = $node->textContent;
        });
        $this->historical = $historical;
    }

    public function isValid(): bool
    {
        return (bool)$this->competitionId;
    }

    public function getCacheValue(): string
    {
        return (string)$this;
    }

    public function getTtl(): ?DateInterval
    {
        return $this->historical ? null : DateInterval::createFromDateString('1 hour');
    }
}
