<?php

namespace PickSuite\PickScraper\Picks;

use DOMNode;
use PickSuite\PickScraper\DOM;

abstract class PickPartial extends DOM
{
    const TD_EXPR = '//td';
    const RANK_AND_TEAM_PATTERN = '/#([0-9]+) on ([A-Za-z\s]+) games/';

    public $type = null;
    public $expertName;
    public $rank;
    public $team;

    public function __construct(string $html)
    {
        parent::__construct($html);
        /**
         * @var int $i
         * @var DOMNode $node
         */
        foreach ($this->xPath->query(static::TD_EXPR) as $i => $node) {
            switch ($i) {
                case 0:
                    $this->expertName = $node->textContent;
                    break;
                case 1:
                    $this->parsePick(trim($node->textContent));
                    break;
                case 2:
                    preg_match(static::RANK_AND_TEAM_PATTERN, $node->textContent, $matches);
                    [$this->rank, $this->team] = array_slice($matches, 1) + [null, null];
                    $this->rank = intval($this->rank);
                    break;
            }
        }
    }

    abstract protected function parsePick(string $text): void;

    public function isValid(): bool
    {
        return !in_array(null, [$this->expertName, $this->team, $this->rank])
            && $this->xPath->query('//td')->length === 3;
    }
}
