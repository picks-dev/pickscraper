<?php

namespace PickSuite\PickScraper\Picks;

class PickSidePartial extends PickPartial
{
    const PICK_PATTERN = '/([A-Z]+)\s*([+\-0-9]+)/';

    public $type = 'side';
    public $pick;
    public $moneyLine;

    public function isValid(): bool
    {
        return parent::isValid() && !in_array(null, [$this->pick, $this->moneyLine]);
    }

    protected function parsePick(string $text): void
    {
        preg_match(static::PICK_PATTERN, $text, $matches);
        [$this->pick, $this->moneyLine] = array_slice($matches, 1) + [null, null];
        $this->moneyLine = floatval($this->moneyLine);
    }
}
