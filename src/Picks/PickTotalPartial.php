<?php

namespace PickSuite\PickScraper\Picks;

class PickTotalPartial extends PickPartial
{
    public $type = 'total';
    public $total;

    public function isValid(): bool
    {
        return parent::isValid() && $this->total !== null;
    }

    protected function parsePick(string $text): void
    {
        $this->total = floatval($text);
    }
}
