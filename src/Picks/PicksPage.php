<?php

namespace PickSuite\PickScraper\Picks;

use DateInterval;
use DOMNode;
use PickSuite\PickScraper\Cacheable;
use PickSuite\PickScraper\DOM;

class PicksPage extends DOM implements Cacheable
{
    const TABLE_EXPR = '//table';
    const TR_EXPR = './/tr[position()>1]';

    public $picks = [];
    private $historical;

    public function __construct(string $html, bool $historical = false)
    {
        parent::__construct($html);
        /**
         * @var int $i
         * @var DOMNode $table
         */
        foreach ($this->xPath->query(static::TABLE_EXPR) as $i => $table) {
            /** @var DOMNode $tr */
            foreach ($this->xPath->query(static::TR_EXPR, $table) as $tr) {
                $this->picks[] = ($i == 0 || $i == 1)
                    ? new PickSidePartial($this->dom->saveHTML($tr))
                    : new PickTotalPartial($this->dom->saveHTML($tr));
            }
        }
        $this->historical = $historical;
    }

    public static function URL(string $competitionId): string
    {
        return "https://contests.covers.com/Consensus/MatchupConsensusExpertDetails/${competitionId}";
    }

    public function isValid(): bool
    {
        return array_reduce($this->picks, function (bool $acc, PickPartial $partial) {
            return $acc && $partial->isValid();
        }, true);
    }

    public function getCacheValue(): string
    {
        return (string)$this;
    }

    public function getTtl(): ?DateInterval
    {
        return $this->historical ? null : DateInterval::createFromDateString('30 minutes');
    }
}
