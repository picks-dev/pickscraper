<?php

namespace PickSuite\PickScraper;

use DateInterval;

interface Cache
{
    public function has(string $key): bool;

    public function get(string $key): ?string;

    public function put(string $key, string $value, ?DateInterval $ttl): void;
}
