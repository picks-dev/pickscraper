<?php

namespace PickSuite\PickScraper\Games;

use DateInterval;
use DateTime;
use DOMNode;
use PickSuite\PickScraper\Cacheable;
use PickSuite\PickScraper\DOM;

class GamesPage extends DOM implements Cacheable
{
    const DATE_EXPR = "//a[@class=\"cmg_active_navigation_item\"]/@data-date";
    const DATE_PATTERN = "%[0-9]{4}-[0-9]{2}-[0-9]{2}%";
    const GAMES_EXPR = "//div[contains(@class,\"cmg_matchup_game_box\")]";
    const GAMES_PATTERN = '/.+/';

    /** @var DateTime */
    public $date;
    /** @var array */
    public $games = [];
    public $historical;
    /** @var DateTime */
    private $targetDate;

    public function __construct(string $html, DateTime $targetDate)
    {
        parent::__construct($html);
        $this->seek(static::DATE_EXPR, static::DATE_PATTERN, function (DOMNode $node) {
            $this->date = new DateTime($node->textContent);
        });
        $this->seek(static::GAMES_EXPR, static::GAMES_PATTERN, function (DOMNode $node) {
            $this->games[] = new GamePartial($this->dom->saveHTML($node));
        });
        $this->targetDate = $targetDate;
        $diff = $this->date->diff(new DateTime);
        $this->historical = $diff->invert === 0 && $diff->days >= 2;
    }

    public static function URL(string $sportAbbr, DateTime $date): string
    {
        return "https://www.covers.com/sports/{$sportAbbr}/matchups?selectedDate=" . $date->format('Y-m-d');
    }

    public function isValid(): bool
    {
        return $this->targetDate->getTimestamp() === $this->date->getTimestamp()
            && array_reduce($this->games, function (bool $acc, GamePartial $partial) {
                return $acc && $partial->isValid();
            }, !empty($this->games));
    }

    public function getCacheValue(): string
    {
        return (string)$this;
    }

    public function getTtl(): ?DateInterval
    {
        return $this->historical ? null : DateInterval::createFromDateString('1 hour');
    }
}
