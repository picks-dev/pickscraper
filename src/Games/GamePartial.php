<?php

namespace PickSuite\PickScraper\Games;

use DateTime;
use DOMNode;
use PickSuite\PickScraper\DOM;

class GamePartial extends DOM
{
    const EVENT_ID_EXPR = "//@data-event-id";
    const EVENT_ID_PATTERN = "%^[0-9]+$%";
    const TEAM_ABBR_EXPR = "//div[@class=\"cmg_team_name\"]/text()";
    const TEAM_ABBR_PATTERN = "%([A-Za-z]+)%";
    const START_TIME_EXPR = "//@data-game-date";
    const START_TIME_PATTERN = "%[0-9\-]{10} [0-9\:]{8}%";
    const STATUS_EXPR = "//div[@class=\"cmg_matchup_list_status\"]";
    const STATUS_PATTERN = "%final%i";
    const CONSENSUS_EXPR = "div[contains(@class, \"cmg_matchup_list_gamebox\")]/a[text()=\"Consensus\"]/@href";
    const CONSENSUS_PATTERN = "%^http%";

    /** @var string */
    public $eventId;
    /** @var string[] */
    public $teamAbbrs = [];
    /** @var DateTime */
    public $startTime;
    /** @var string */
    public $status;
    /** @var float[] */
    public $awayScores = [];
    /** @var float[] */
    public $homeScores = [];
    /** @var int */
    public $overUnder;
    /** @var string */
    public $consensusHref;

    public function __construct(string $html)
    {
        parent::__construct($html);
        $this->seek(static::EVENT_ID_EXPR, static::EVENT_ID_PATTERN, function (DOMNode $node) {
            $this->eventId = $node->textContent;
        });
        $this->seek(static::TEAM_ABBR_EXPR, static::TEAM_ABBR_PATTERN, function (DOMNode $node, array $matches) {
            $this->teamAbbrs[] = end($matches);
        });
        $this->seek(static::START_TIME_EXPR, static::START_TIME_PATTERN, function (DOMNode $node) {
            $this->startTime = new DateTime($node->textContent);
        });
        $this->seek(static::STATUS_EXPR, static::STATUS_PATTERN, function (DOMNode $node) {
            $this->status = $node->textContent;
        });
        $this->seek(static::CONSENSUS_EXPR, static::CONSENSUS_PATTERN, function (DOMNode $node) {
            $this->consensusHref = $node->textContent;
        });
        /** @var DOMNode $node */
        foreach ($this->xPath->query("//table//tr[1]/th") as $i => $node) {
            $i++;
            $away = $this->xPath->query("//table/tbody/tr[1]/td[{$i}]")->item(0);
            $home = $this->xPath->query("//table/tbody/tr[2]/td[{$i}]")->item(0);

            if (is_numeric($node->textContent)) {
                if ($away) {
                    $this->awayScores[] = floatval($away->textContent);
                }
                if ($home) {
                    $this->homeScores[] = floatval($home->textContent);
                }
            }
            if ($node->textContent === "O/U") {
                $this->overUnder = intval($away->textContent ?: $home->textContent);
            }
        }
    }

    public function isValid(): bool
    {
        return $this->eventId && $this->status && $this->startTime
            && !empty($this->teamAbbrs) && !empty($this->awayScores) && !empty($this->homeScores);
    }
}
