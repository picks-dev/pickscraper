<?php

namespace PickSuite\PickScraper;

use DateInterval;

interface Cacheable
{
    public function isValid(): bool;

    public function getCacheValue(): string;

    public function getTtl(): ?DateInterval;
}
