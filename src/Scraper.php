<?php

namespace PickSuite\PickScraper;

use DateTime;
use PickSuite\PickScraper\Games\GamePartial;
use PickSuite\PickScraper\Games\GamesPage;
use PickSuite\PickScraper\Picks\ConsensusPage;
use PickSuite\PickScraper\Picks\PicksPage;
use PickSuite\PickScraper\Teams\MLBTeamPage;
use PickSuite\PickScraper\Teams\MLBTeamsPage;
use PickSuite\PickScraper\Teams\TeamPage;
use PickSuite\PickScraper\Teams\TeamsPage;

class Scraper
{
    /** @var HttpTransport */
    private $transport;
    /** @var Cache */
    private $cache;

    public function __construct(HttpTransport $transport, Cache $cache)
    {
        $this->transport = $transport;
        $this->cache = $cache;
    }

    /**
     * @param string $sportAbbr
     * @return TeamPage[]
     */
    public function scrapeTeams(string $sportAbbr): array
    {
        $url = TeamsPage::URL($sportAbbr);
        $html = $this->fetch($url);
        switch ($sportAbbr) {
            case 'mlb':
            case 'MLB':
                $page = new MLBTeamsPage($html);
                break;
            default:
                $page = new TeamsPage($html);
        }
        $this->cache($url, $page);
        return array_map(function (string $teamHref) use ($sportAbbr) {
            $url = TeamPage::URL($teamHref);
            $html = $this->fetch($url);
            switch ($sportAbbr) {
                case 'mlb':
                case 'MLB':
                    $page = new MLBTeamPage($html);
                    break;
                default:
                    $page = new TeamPage($html);
            }
            $this->cache($url, $page);
            return $page;
        }, $page->teamHrefs);
    }

    public function fetch(string $url): string
    {
        if (!$this->cache->has($url)) {
            trigger_error("Cache miss: {$url}");
        }
        return $this->cache->has($url)
            ? $this->cache->get($url)
            : $this->transport->one($url);
    }

    public function cache(string $key, Cacheable $value): void
    {
        if ($value->isValid()) {
            $this->cache->put($key, $value->getCacheValue(), $value->getTtl());
        }
    }

    public function scrapeGames(string $sportAbbr, DateTime $date): GamesPage
    {
        $url = GamesPage::URL($sportAbbr, $date);
        $html = $this->fetch($url);
        $page = new GamesPage($html, $date);
        $this->cache($url, $page);

        return $page;
    }

    public function scrapePicks(GamePartial $game, bool $historical = false)
    {
        $html = $this->fetch($game->consensusHref);
        $page = new ConsensusPage($html, $historical);
        $this->cache($game->consensusHref, $page);
        $url = PicksPage::URL($page->competitionId);
        $html = $this->fetch($url);
        $page = new PicksPage($html, $historical);
        $this->cache($url, $page);
        return $page;
    }
}
