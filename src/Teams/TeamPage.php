<?php

namespace PickSuite\PickScraper\Teams;

use DateInterval;
use DOMNode;
use PickSuite\PickScraper\Cacheable;
use PickSuite\PickScraper\DOM;

class TeamPage extends DOM implements Cacheable
{
    const ABBR_EXPR = "//img[@alt=\"TeamLogo\"]/@src";
    const ABBR_PATTERN = "%^(https:)?//images\..+/([a-z]+)\.[a-z]+$%";
    const NAME_EXPR = "//title";
    const NAME_PATTERN = "%^\W*([\w ]+) -.*$%";

    /** @var string */
    public $abbr;
    /** @var string */
    public $name;

    public function __construct(string $html)
    {
        parent::__construct($html);
        $this->seek(static::ABBR_EXPR, static::ABBR_PATTERN, function (DOMNode $node, array $matches) {
            $this->abbr = end($matches);
        });
        $this->seek(static::NAME_EXPR, static::NAME_PATTERN, function (DOMNode $node, array $matches) {
            $this->name = end($matches);
        });
    }

    public static function URL(string $teamHref)
    {
        return "https://www.covers.com{$teamHref}";
    }

    public function isValid(): bool
    {
        return $this->abbr && $this->name;
    }

    public function getCacheValue(): string
    {
        return (string)$this;
    }

    public function getTtl(): ?DateInterval
    {
        return DateInterval::createFromDateString('1 month');
    }
}
