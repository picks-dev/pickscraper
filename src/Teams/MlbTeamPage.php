<?php

namespace PickSuite\PickScraper\Teams;

class MLBTeamPage extends TeamPage
{
    const ABBR_EXPR = "//div[@class=\"teamlogo\"]/img/@src";
    const NAME_PATTERN = "%^\W*([\w ]+) team.*$%";
}
