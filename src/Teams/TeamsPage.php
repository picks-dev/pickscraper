<?php

namespace PickSuite\PickScraper\Teams;

use DateInterval;
use DOMNode;
use PickSuite\PickScraper\Cacheable;
use PickSuite\PickScraper\DOM;

class TeamsPage extends DOM implements Cacheable
{
    const LINKS_EXPR = "//a/@href";
    const LINKS_PATTERN = "%/sport/[\w\-]+/\w+/teams/main/[\w\-]+/[0-9\-]+%";
    /** @var string[] */
    public $teamHrefs = [];

    public function __construct(string $html)
    {
        parent::__construct($html);
        $this->seek(static::LINKS_EXPR, static::LINKS_PATTERN, function (DOMNode $node) {
            $this->teamHrefs[] = $node->textContent;
        });
    }

    public static function URL(string $sportAbbr): string
    {
        return "https://www.covers.com/pageLoader/pageLoader.aspx?page=/data/{$sportAbbr}/teams/teams.html";
    }

    public function isValid(): bool
    {
        return !empty($this->teamHrefs);
    }

    public function getCacheValue(): string
    {
        return (string)$this;
    }

    public function getTtl(): ?DateInterval
    {
        return DateInterval::createFromDateString('1 month');
    }
}
