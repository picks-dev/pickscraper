<?php

namespace PickSuite\PickScraper\Teams;

class MLBTeamsPage extends TeamsPage
{
    const LINKS_PATTERN = "%/pageLoader/pageLoader\.aspx\?page=/data/mlb/teams/team[0-9]+\.html%";
}
