<?php

namespace PickSuite\PickScraper\Impl;

use PickSuite\PickScraper\HttpTransport;

class CurlTransport implements HttpTransport
{
    const CURL_OPTS = [
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_RETURNTRANSFER => true,
    ];

    public function one(string $url): string
    {
        $ch = curl_init($url);
        curl_setopt_array($ch, static::CURL_OPTS);
        $res = curl_exec($ch);
        $curlError = curl_error($ch);
        if ($curlError !== '') {
            trigger_error("Curl error: {$curlError}" . PHP_EOL, E_USER_WARNING);
        }
        curl_close($ch);
        return $res;
    }

    public function many(string ...$urls): array
    {
        $res = [];
        $ch = [];
        $mh = curl_multi_init();

        foreach ($urls as $i => $url) {
            $ch[$i] = curl_init($url);
            curl_setopt_array($ch[$i], static::CURL_OPTS);
            curl_multi_add_handle($mh, $ch[$i]);
        }

        do {
            if (curl_multi_select($mh) !== -1) {
                usleep(100);
            }

            do {
                $execReturnValue = curl_multi_exec($mh, $runningHandles);
            } while ($execReturnValue === CURLM_CALL_MULTI_PERFORM);
        } while ($runningHandles && $execReturnValue === CURLM_OK);

        if ($execReturnValue != CURLM_OK) {
            trigger_error("Curl multi read error {$execReturnValue}" . PHP_EOL, E_USER_WARNING);
        }

        foreach ($urls as $i => $url) {
            if (!array_key_exists($i, $ch)) {
                continue;
            }
            $curlError = curl_error($ch[$i]);
            if ($curlError === '') {
                $responseContent = curl_multi_getcontent($ch[$i]);
                $res[$i] = $responseContent;
            } else {
                trigger_error("Curl error on handle {$i}: {$curlError}" . PHP_EOL, E_USER_WARNING);
            }
            curl_multi_remove_handle($mh, $ch[$i]);
            curl_close($ch[$i]);
        }

        curl_multi_close($mh);
        return $res;
    }
}
