<?php

namespace PickSuite\PickScraper\Impl;

use DateInterval;
use DateTime;
use Exception;
use PickSuite\PickScraper\Cache;

class FileCache implements Cache
{
    /** @var array[int, string] */
    private static $MEM_CACHE = [];
    /** @var string */
    private $dir;

    public function __construct(string $dir = null)
    {
        if (!($this->dir = $this->ensureDir($dir ?: sys_get_temp_dir()))) {
            throw new Exception("Could not ensure cache dir: {$dir}");
        }
    }

    private function ensureDir(string $dir): ?string
    {
        return is_dir($dir) || mkdir($dir, 0777, true) ? $dir : null;
    }

    public function has(string $key): bool
    {
        $ttl = $this->read($key)[0];
        return $ttl === -1 || $ttl > time();
    }

    private function read(string $key): array
    {
        $key = sha1($key);
        if (array_key_exists($key, static::$MEM_CACHE)) {
            return static::$MEM_CACHE[$key];
        }

        $filename = $this->dir . '/' . sha1($key);
        $value = file_exists($filename)
            ? json_decode(file_get_contents($filename), true)
            : [null, null];
        return static::$MEM_CACHE[$key] = $value;
    }

    public function get(string $key): ?string
    {
        return $this->read($key)[1] ?? null;
    }

    public function put(string $key, string $value, ?DateInterval $ttl): void
    {
        $ttl = $ttl ? (new DateTime)->add($ttl)->getTimestamp() : -1;
        $this->write($key, [$ttl, $value]);
    }

    private function write(string $key, array $value)
    {
        $key = sha1($key);
        file_put_contents($this->dir . '/' . sha1($key), json_encode($value));
        static::$MEM_CACHE[$key] = $value;
    }
}
