<?php

namespace PickSuite\PickScraper;

use DOMDocument;
use DOMXPath;

class DOM
{
    protected $dom;
    protected $xPath;

    public function __construct(string $html)
    {
        $this->dom = new DOMDocument;
        $useInternalErrors = libxml_use_internal_errors(true);
        $this->dom->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        libxml_use_internal_errors($useInternalErrors);
        $this->xPath = new DOMXPath($this->dom);
    }

    protected static function equals(string $value, string $attr = 'id'): string
    {
        return "[@{$attr}=\"{$value}\"]";
    }

    protected static function contains(string $value, string $attr = 'class'): string
    {
        return "[contains(@{$attr},\"{$value}\")]";
    }

    public function __toString(): string
    {
        return $this->dom->saveHTML();
    }

    protected function seek(string $expr, string $pattern, callable $cb)
    {
        foreach ($this->xPath->query($expr) as $node) {
            if (preg_match($pattern, $node->textContent, $matches)) {
                if ($cb($node, $matches) === false) {
                    return;
                }
            }
        }
    }
}
