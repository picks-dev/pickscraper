<?php

namespace PickSuite\PickScraper;

interface HttpTransport
{
    public function one(string $url): string;

    public function many(string ...$urls): array;
}
