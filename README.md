# PickSuite/PickScraper

A lightweight library for scraping data from [covers.com](https://www.covers.com/).

# Vocabulary

* Page: the entirety of a web page's  HTML
* Partial: a subset of HTML inside a web page

# Library Structure

### [src](./src) 

* [Scraper](./src/Scraper.php) is an API to the entire application, providing a default implementation of how to scrape various pages and partials.
* [DOM](./src/DOM.php) is a wrapper around DOMDocument and DOMXPath and acts as a base class for the various Pages and Partials.

### [Impl](./src/Impl)

These are partial or complete implementations of the various scraper interfaces, provided for convenience.

* [DomParser](./src/Impl/DomParser.php) is a very simple wrapper on top of the DOMDocument/DOMXpath/DOMNode PHP classes. It provides a certain amount of convenience in parsing html.
* [PickScraper](./src/Impl/PickScraper.php) composes an [HttpClient](./src/HttpClient.php) and [HtmlCache](./src/HtmlCache.php) in order to fetch and cache HTML from the web (or a cache) so that it can be parsed by an [DOM](./src/DOM.php).

### [Teams](./src/Teams)

These are implementations of the [DOM](./src/DOM.php) interface written for parsing teams data;

* [TeamsPage](./src/Teams/TeamsPage.php) simply fetches the URLs to pages with specific data about the various sports teams.
* [TeamPage](./src/Teams/TeamPage.php) is what actually models team data.

Because [covers.com](https://www.covers.com) handles Major League Baseball in a unique way, that sport's teams need custom DOMs.

### [Games](./src/Games)

Because covers.com will redirect to the last valid date for games in the given season, [GamesPage](./src/Games/GamesPage.php) validates the target date, as well as compiling [GameBoxPartials](./src/Games/GameBoxPartial.php). The games page uses this date to determine if a scrape is 'historical', meaning the results should be cached forever.

### [Picks](./src/Picks)

* [ConsensusPage](./src/Picks/ConsensusPage.php) merely looks up the URL to fetch the game's table of picks, which is loaded via XHr.
* [PicksPartial](./src/Picks/PicksPage.php) simply delegates to [PickPartial](./src/Picks/PickPartial.php), providing context on which of the 4 tables each pick exists in.

# Usage

Refer to [index.php](./index.php) for an example implementation of this library.
