<pre>
<?php

use PickSuite\PickScraper\Games\GamePartial;
use PickSuite\PickScraper\Impl\CurlTransport;
use PickSuite\PickScraper\Impl\FileCache;
use PickSuite\PickScraper\Scraper;

require __DIR__ . '/vendor/autoload.php';

$transport = new CurlTransport;
$cache = new FileCache(__DIR__ . '/cache');
$scraper = new Scraper($transport, $cache);

$sportAbbr = "nhl";
$targetDate = new DateTime("2020/01/14");

$data = [
    'teams' => $teams = $scraper->scrapeTeams($sportAbbr),
    'games' => $games = $scraper->scrapeGames($sportAbbr, $targetDate),
    'picks' => $picks = array_combine(
        array_map(function (GamePartial $partial) {
            return $partial->eventId;
        }, $games->games),
        array_map(function (GamePartial $partial) use ($scraper, $games) {
            return $scraper->scrapePicks($partial, $games->historical);
        }, $games->games)
    ),
];

echo json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
